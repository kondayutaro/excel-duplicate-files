# Excel Duplicate Row Finder

## Functionalities
* Find a row containing duplicate value, and colour the cell

## How to Use
* Set excel workbook path to `config.py`
* Set excel worksheet to work on to `config.py`
* Specify column letter in `config.py`
* `pipenv install`
* `pipenv shell`
* `py __main__.py` for Windows
