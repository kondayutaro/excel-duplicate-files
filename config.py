from dataclasses import dataclass

@dataclass
class Config:
    workbook_name: str = r"C:\Users\konda\Desktop\TestExcel.xlsx"
    sheet_name: str = "TestSheet"
    # The first row or column integer is 0
    column_letter: str = "E"