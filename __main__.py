import openpyxl
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.utils import column_index_from_string
from openpyxl.styles import PatternFill
from config import Config

# Load config
config = Config()

# Open excel workbook
try:
    wb: Workbook = openpyxl.load_workbook(config.workbook_name)
except FileNotFoundError:
    print(f"{config.workbook_name} not found. Terminating")
    exit(1)

# Open excel sheet
try:
    sheet: Worksheet = wb[config.sheet_name]
except KeyError:
    print(f"{config.sheet_name} not found. Terminating")
    exit(1)

column_index = column_index_from_string(config.column_letter)
cells_in_column = list(sheet.columns)[column_index - 1]
cell_list: list[(int, str)] = []
for cell in cells_in_column:
    cell_list.append((cell.row, cell.value))

# Sort by value
cell_list.sort(key=lambda cell: cell[1])

value: str = ""
for cell in cell_list:
    if value != cell[1]:
        value = cell[1]
        sheet.cell(cell[0], column_index).fill = PatternFill("solid", fgColor="FFFFFF")
    else:
        # Fill cell with colour if a duplicate exists.
        sheet.cell(cell[0], column_index).fill = PatternFill("solid", fgColor="DFFF00")

# This modifies the workbook itself.
wb.save(config.workbook_name)